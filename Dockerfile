FROM java:8-jre
MAINTAINER sumona_bhattacharya1
EXPOSE 8082 8083
COPY maven /maven/
CMD java -jar \
    /target/dropwizard-kafka-http-0.0.1-SNAPSHOT.jar server \
    /kafka-http.yml